set fish_greeting

alias ls='exa -a --color=always --group-directories-first --icons'
alias lsa='exa -al --color=always --group-directories-first --icons'
alias tree='exa -aT --color=always --group-directories-first --icons'

alias install='sudo pacman -S'
alias update='sudo pacman -Syy'
alias upgrade='sudo pacman -Syyuu'

export LFS=/mnt/lfs

set -g EDITOR nvim
# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
cat ~/.cache/wal/sequences &

# Alternative (blocks terminal for 0-3ms)
cat ~/.cache/wal/sequences

# To add support for TTYs this line can be optionally added.
source ~/.cache/wal/colors-tty.sh

if status is-interactive
	#python3 ~/.config/fish/greeter.py | lolcat
	echo "Hello $USER"
	echo (date) | lolcat
    # Commands to run in interactive sessions can go here
end
